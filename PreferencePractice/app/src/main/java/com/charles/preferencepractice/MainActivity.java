package com.charles.preferencepractice;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView tv;
    private Button save, refresh;
    private EditText input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input = (EditText) findViewById(R.id.input);
        tv = (TextView) findViewById(R.id.tv);
        save = (Button) findViewById(R.id.save);
        refresh = (Button) findViewById(R.id.refresh);

        refresh();


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save();
            }
        });

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refresh();
            }
        });
    }

    public void refresh() {
        String str = PrefManager.getInstance().load(this);
        tv.setText(str);
    }

    public void save() {
        String inputText = input.getText().toString();
        PrefManager.getInstance().save(this, inputText);
    }
}
