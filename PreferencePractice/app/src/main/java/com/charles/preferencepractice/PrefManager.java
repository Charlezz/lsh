package com.charles.preferencepractice;

import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Created by Charles on 2017. 1. 22..
 */
public class PrefManager {

    public static final String TAG = PrefManager.class.getSimpleName();

    private static PrefManager ourInstance = new PrefManager();

    public static PrefManager getInstance() {
        return ourInstance;
    }

    private PrefManager() {
    }

    public void save(Context context, String str) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("data", str).commit();
    }

    public String load(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("data", "");
    }
}
