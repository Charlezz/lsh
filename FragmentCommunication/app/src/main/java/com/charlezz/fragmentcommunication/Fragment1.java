package com.charlezz.fragmentcommunication;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Charles on 2017. 1. 31..
 */

public class Fragment1 extends Fragment {
    public static Fragment1 newInstance() {
        return new Fragment1();
    }


    EditText editText;
    Button send;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.input, null);
        editText = (EditText) view.findViewById(R.id.editText);
        send = (Button) view.findViewById(R.id.send);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = editText.getText().toString();
                ((MainActivity) getActivity()).send(str);
            }
        });
        return view;
    }
}
