package com.charlezz.fragmentcommunication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Charles on 2017. 1. 31..
 */

public class Fragment2 extends Fragment {
    public static Fragment2 newInstance() {
        return new Fragment2();
    }

    TextView tv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        tv = new TextView(getActivity());
        return tv;
    }

    public void showText(String str) {
        tv.setText(str);
    }
}
