package com.charlezz.fragmentcommunication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    Fragment1 fragment1;
    Fragment2 fragment2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragment1 = (Fragment1) getSupportFragmentManager().findFragmentByTag("f1");
        fragment2 = (Fragment2) getSupportFragmentManager().findFragmentByTag("f2");
    }

    public void send(String str){
        fragment2.showText(str);
    }
}
