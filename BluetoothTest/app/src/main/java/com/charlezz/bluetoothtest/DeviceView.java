package com.charlezz.bluetoothtest;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Charles on 2017. 5. 14..
 */

public class DeviceView extends FrameLayout {

    @BindView(R.id.device_name)
    TextView deviceName;
    @BindView(R.id.mac_address)
    TextView macAddress;

    private BluetoothDevice device;

    public DeviceView(@NonNull Context context) {
        super(context);
        View view = LayoutInflater.from(context).inflate(R.layout.device_view_layout, this, true);
        ButterKnife.bind(this, view);
    }

    public void setBluetoothDevice(BluetoothDevice device) {
        this.device = device;
        deviceName.setText(device.getName());
        macAddress.setText(device.getAddress());
    }
}
