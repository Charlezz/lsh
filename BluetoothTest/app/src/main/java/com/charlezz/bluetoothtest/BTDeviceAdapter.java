package com.charlezz.bluetoothtest;

import android.bluetooth.BluetoothDevice;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by Charles on 2017. 5. 14..
 */

public class BTDeviceAdapter extends BaseAdapter {

    private ArrayList<BluetoothDevice> items = new ArrayList<>();


    public void add(BluetoothDevice item) {
        if (!items.contains(item)) {
            items.add(item);
            notifyDataSetChanged();
        }
    }

    public void setItems(ArrayList<BluetoothDevice> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public BluetoothDevice getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DeviceView view = null;
        if (convertView == null) {
            view = new DeviceView(parent.getContext());
        } else {
            view = (DeviceView) convertView;
        }
        view.setBluetoothDevice(items.get(position));
        return view;
    }
}
