package com.charlezz.butterknifetest;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.btn0)
    Button btn0;

    @BindView(R.id.edit)
    EditText edit;

    @BindView(R.id.btn1)
    Button btn1;

    @BindView(R.id.btn2)
    Button btn2;

    @BindViews({R.id.btn1, R.id.btn2, R.id.edit})
    List<View> viewList;

    @BindString(R.string.name)
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

//        String name = getResources().getString(R.string.name);
        Log.e(TAG, "name:" + name);
    }


    boolean isShown = false;

    @OnClick(R.id.btn0)
    public void showMyToast(Button btn) {
        btn.setText("hello");
        Toast.makeText(MainActivity.this, edit.getText().toString(), Toast.LENGTH_SHORT).show();

        isShown = !isShown;

//        if (isShown) {
//            edit.setVisibility(View.INVISIBLE);
//            btn1.setVisibility(View.INVISIBLE);
//            btn2.setVisibility(View.INVISIBLE);
//        } else {
//            edit.setVisibility(View.VISIBLE);
//            btn1.setVisibility(View.VISIBLE);
//            btn2.setVisibility(View.VISIBLE);
//        }

        ButterKnife.apply(viewList, new ButterKnife.Action<View>() {
            @Override
            public void apply(@NonNull View view, int index) {
                if (!isShown) {
                    view.setVisibility(View.VISIBLE);
                } else {
                    view.setVisibility(View.INVISIBLE);
                }

            }
        });


    }

    private Unbinder unbinder;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
