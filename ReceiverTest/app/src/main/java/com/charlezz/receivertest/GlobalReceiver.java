package com.charlezz.receivertest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Charles on 2017. 1. 31..
 */

public class GlobalReceiver extends BroadcastReceiver {
    public static final String TAG = GlobalReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "onReceive from global");
        String action = intent.getAction();
        if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {
            Log.e(TAG, "Battery Changed");
        } else if (action.equals(Intent.ACTION_SCREEN_OFF)) {
            Log.e(TAG, "ACTION_SCREEN_OFF");
        } else if (action.equals(Intent.ACTION_SCREEN_ON)) {
            Log.e(TAG, "ACTION_SCREEN_ON");
        } else if (action.equals(Intent.ACTION_POWER_DISCONNECTED)) {
            Log.e(TAG, "ACTION_POWER_DISCONNECTED");
        } else if (action.equals(Intent.ACTION_POWER_CONNECTED)) {
            Log.e(TAG, "ACTION_POWER_CONNECTED");
        } else if (action.equals(Intent.ACTION_BOOT_COMPLETED)){
            Log.e(TAG, "ACTION_BOOT_COMPLETED");
        }
    }
}
