package com.charles.basicfragment.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.charles.basicfragment.R;

/**
 * Created by Charles on 2017. 1. 15..
 */

public class MyFragment extends Fragment {

    public static final String KEY = "MyFragment key";

    public static final int ID_SH = 0;
    public static final int ID_IU = 1;
    public static final int ID_ZW = 2;


    public static MyFragment newInstance(int id) {
        //번들객체 생성
        Bundle args = new Bundle();
        args.putInt(KEY, id);
        //프레그먼트 객체 생성
        MyFragment fragment = new MyFragment();
        //프레그먼트에 번들 전달
        fragment.setArguments(args);
        return fragment;
    }

    private int fragmentId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentId = getArguments().getInt(KEY);
    }


    private TextView name;
    private ImageView image;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.myfragment_layout, container, false);
        name = (TextView) view.findViewById(R.id.name);
        image = (ImageView) view.findViewById(R.id.image);

        setData(fragmentId);

        return view;
    }

    public void setData(int id) {
        switch (id) {
            case ID_IU:
                name.setText("IU");
                image.setImageResource(R.drawable.iu);
                break;
            case ID_SH:
                name.setText("Seol Hyun");
                image.setImageResource(R.drawable.sh);
                break;
            case ID_ZW:
                name.setText("TWICE");
                image.setImageResource(R.drawable.zw);
                break;
        }
    }
}
