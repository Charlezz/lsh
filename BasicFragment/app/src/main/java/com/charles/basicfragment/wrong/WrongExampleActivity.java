package com.charles.basicfragment.wrong;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.charles.basicfragment.R;

public class WrongExampleActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView iv_sh, iv_iu, iv_zw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wrong_example);
        findViewById(R.id.sh).setOnClickListener(this);
        findViewById(R.id.iu).setOnClickListener(this);
        findViewById(R.id.zw).setOnClickListener(this);
        iv_sh = (ImageView) findViewById(R.id.iv_sh);
        iv_iu = (ImageView) findViewById(R.id.iv_iu);
        iv_zw = (ImageView) findViewById(R.id.iv_zw);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sh:
                setImage(iv_sh);
                break;
            case R.id.iu:
                setImage(iv_iu);
                break;
            case R.id.zw:
                setImage(iv_zw);
                break;
        }
    }

    public void setImage(ImageView iv) {
        iv_sh.setVisibility(View.GONE);
        iv_iu.setVisibility(View.GONE);
        iv_zw.setVisibility(View.GONE);

        iv.setVisibility(View.VISIBLE);
    }


}
