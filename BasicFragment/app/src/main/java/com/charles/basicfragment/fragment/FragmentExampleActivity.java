package com.charles.basicfragment.fragment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.charles.basicfragment.R;

public class FragmentExampleActivity extends AppCompatActivity implements View.OnClickListener {


    private Button btn0, btn1, btn2;

    private MyFragment myFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_example_ativity);
        myFragment = MyFragment.newInstance(MyFragment.ID_IU);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, myFragment).commit();

        btn0 = (Button) findViewById(R.id.iu);
        btn1 = (Button) findViewById(R.id.sh);
        btn2 = (Button) findViewById(R.id.zw);

        btn0.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
//        if (view.equals(btn0)) {
//            getSupportFragmentManager().beginTransaction().replace(R.id.container, MyFragment.newInstance(MyFragment.ID_IU)).commit();
//        } else if (view.equals(btn1)) {
//            getSupportFragmentManager().beginTransaction().replace(R.id.container, MyFragment.newInstance(MyFragment.ID_SH)).commit();
//        } else if (view.equals(btn2)) {
//            getSupportFragmentManager().beginTransaction().replace(R.id.container, MyFragment.newInstance(MyFragment.ID_ZW)).commit();
//        }

        if (view.equals(btn0)) {
            myFragment.setData(MyFragment.ID_IU);
        } else if (view.equals(btn1)) {
            myFragment.setData(MyFragment.ID_SH);
        } else if (view.equals(btn2)) {
            myFragment.setData(MyFragment.ID_ZW);
        }
    }
}
