package com.charles.basicfragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.charles.basicfragment.fragment.FragmentExampleActivity;
import com.charles.basicfragment.wrong.WrongExampleActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    Button btn1, btn2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn1).setOnClickListener(this);
        findViewById(R.id.btn2).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn1:
                startActivity(new Intent(MainActivity.this, WrongExampleActivity.class));
                break;
            case R.id.btn2:
                startActivity(new Intent(MainActivity.this, FragmentExampleActivity.class));
                break;
        }

    }
}
