package com.charles.viewpagerpractice;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by Charles on 2017. 1. 22..
 */

public class MyFragment extends Fragment {


    public static final String TAG = MyFragment.class.getSimpleName();
    public static final String KEY = "myfragment key";
    private int id;

    public static MyFragment newInstance(int id) {

        Bundle args = new Bundle();
        args.putInt(KEY, id);
        MyFragment fragment = new MyFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        id = getArguments().getInt(KEY);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.myfragment_layout, container, false);
        TextView tv = (TextView) view.findViewById(R.id.tv);
        Random rnd = new Random();
        tv.setBackgroundColor(Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));
        tv.setText(String.valueOf(id));
        return view;
    }
}
