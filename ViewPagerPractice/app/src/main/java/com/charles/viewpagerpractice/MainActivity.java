package com.charles.viewpagerpractice;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private MyPagerAdapter adapter;
    private ViewPager pager;
    private TabLayout tabLayout;

//    private EditText editNum;
//    private Button change;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        pager = (ViewPager) findViewById(R.id.pager);
        adapter = new MyPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(pager, true);


        tabLayout.setTabMode(TabLayout.MODE_FIXED);

//        tabLayout.getTabAt(0).setIcon(R.mipmap.ic_launcher);

//        change = (Button) findViewById(change);
//        editNum = (EditText) findViewById(editNum);
//
//        change.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String strNum = editNum.getText().toString();
//                int num = Integer.parseInt(strNum);
//                pager.setCurrentItem(num, true);
//            }
//        });
    }
}
