package com.charlezz.chartlibrarypractice;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.chart)
    LineChart chart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        chart.setDrawGridBackground(false);
        chart.setGridBackgroundColor(R.color.colorAccent);
        chart.getDescription().setEnabled(false);
        chart.setTouchEnabled(true);

        // enable scaling and dragging
//        chart.setDragEnabled(true);
//        chart.setScaleEnabled(true);
//        chart.getAxisLeft().setEnabled(true);
//        chart.getAxisLeft().setSpaceTop(40);
//        chart.getAxisLeft().setSpaceBottom(40);
        chart.getAxisRight().setEnabled(false);
        chart.getXAxis().setEnabled(false);

        // if disabled, scaling can be done on x- and y-axis separately
//        chart.setPinchZoom(false);
//        chart.setViewPortOffsets(10, 0, 10, 0);

        chart.setBackgroundColor(Color.rgb(250, 104, 104));

        List<Entry> entries = new ArrayList<Entry>();

        for (int i = 0; i < 5; i++) {
            Entry entry = new Entry(i, i);
            entries.add(entry);
        }

        LineDataSet dataSet = new LineDataSet(entries, "Title");
        LineData data = new LineData(dataSet);

        chart.animateX(1000 * 1);
        chart.setData(data);
        chart.invalidate();


    }


}
