package com.charlezz.chattest;

import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Charles on 2017. 5. 7..
 */

public class ChatConnection {
    public static final String TAG = ChatConnection.class.getSimpleName();

    public static final int PORT = 9998;

    public static final int STATE_NONE = 0;//아무일도 하지 않을때
    public static final int STATE_LISTEN = 1;//서버가 대기중일때
    public static final int STATE_CONNNECTING = 2;//서버와 클라이언트가 연결시도중일때
    public static final int STATE_CONNECTED = 3;//연결되었을때


    //Member Fields
    private int mState;
    private Handler mHandler;

    private AcceptThread mAcceptThread;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;

    public ChatConnection(Handler handler) {
        mState = STATE_NONE;
        mHandler = handler;
    }

    private synchronized void setState(int state) {
        Log.e(TAG, "State changed : " + mState + "->" + state);
        mState = state;
        mHandler.obtainMessage(Constant.MESSAGE_STATE_CHANGED, state, -1).sendToTarget();
    }

    public synchronized int getState() {
        return mState;
    }

    public synchronized void start() {
        Log.e(TAG, "start");
        //open server

        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        setState(STATE_LISTEN);
        if (mAcceptThread == null) {
            mAcceptThread = new AcceptThread();
            mAcceptThread.start();
        }

    }

    public synchronized void connect(String ip) {

        //새로운 연결을 위해 기존연결 끊어줌
        if (mState == STATE_CONNNECTING) {
            if (mConnectThread != null) {
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }

        //이미 돌아가는 쓰레드가 있다면 또 끊어줌
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        //새로운 연결 시작
        mConnectThread = new ConnectThread(ip);
        mConnectThread.start();
        setState(STATE_CONNNECTING);
    }

    public synchronized void connected(Socket socket) {
        if (mConnectThread != null) {
            Log.e(TAG, "cancel call in connected()");
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        if (mAcceptThread != null) {
            mAcceptThread.cancel();
            mAcceptThread = null;
        }

        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();

        // Send the name of the connected device back to the UI Activity
        mHandler.obtainMessage(Constant.MESSAGE_TOAST, socket.getLocalAddress().getHostAddress());

        setState(STATE_CONNECTED);
    }

    public synchronized void stop() {
        Log.e(TAG, "stop");
        //close server and client
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        if (mAcceptThread != null) {
            mAcceptThread.cancel();
            mAcceptThread = null;
        }
        setState(STATE_NONE);
    }


    public void write(byte[] out) {
        synchronized (this) {
            if (mState != STATE_CONNECTED) {
                Log.e(TAG, "write() is called when Not STATE_CONNECTED");
                return;
            }
            mConnectedThread.write(out);
        }
    }

    private void connectionFailed() {
        Log.e(TAG, "connectionFailed() called");
        mHandler.obtainMessage(Constant.MESSAGE_TOAST, new String("접속 할수 없습니다.")).sendToTarget();
        //접속 실패 했으므로 서버 재가동
        ChatConnection.this.start();
    }

    private void connectionLost() {
        Log.e(TAG, "connectionLost() called");
        mHandler.obtainMessage(Constant.MESSAGE_TOAST, new String("접속이 끊겼습니다")).sendToTarget();
        //접속 유실 했으므로 서버 재가동
        ChatConnection.this.start();
    }

    //서버전용 쓰레드
    private class AcceptThread extends Thread {
        //server socket
        private ServerSocket mServerSocket;

        public AcceptThread() {
            try {
                mServerSocket = new ServerSocket(PORT);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            super.run();
            Socket socket = null;
            while (mState != STATE_CONNECTED) {
                try {
                    socket = mServerSocket.accept();//블락킹 콜, 연결이 정상적으로 되거나, 비정상적으로 연결시도가 실패될때만 리턴값이 넘어옴
                } catch (IOException e) {
                    //연결실패시 에러 출력
                    e.printStackTrace();
                    break;
                }

                //만약 위에서 블락킹 콜이 성공적으로 클라이언트 소켓과 연결이 성공적으로 이루어질경우 이쪽 라인으로 들어옴
                if (socket != null) {
                    synchronized (ChatConnection.this) {
                        switch (mState) {
                            case STATE_LISTEN:
                            case STATE_CONNNECTING:
                                connected(socket);
                                break;
                            case STATE_NONE:
                            case STATE_CONNECTED:
                                try {
                                    socket.close();
                                } catch (IOException e) {
                                    //소켓닫기 실패할경우 이곳
                                    e.printStackTrace();
                                }
                                break;
                        }
                    }
                }
            }
            //Accepted Thread 작업 끝남
        }

        public void cancel() {
            try {
                mServerSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //클라이언트 전용 쓰레드
    private class ConnectThread extends Thread {
        private Socket mSocket;
        private String ip;

        public ConnectThread(String ip) {
            this.ip = ip;
        }

        @Override
        public void run() {
            super.run();
            try {
                mSocket = new Socket(ip, PORT);//ip주소를 통해 서버로 연결시도
            } catch (IOException e) {
                try {
                    mSocket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();

                }
                e.printStackTrace();
                connectionFailed();
                return;
            }

            //정상적으로 서버에 접속시 이곳으로 진입

            synchronized (this) {
                mConnectThread = null;//쓰레드를 null로 바꾸어 connected()에서 이 쓰레드내의 cancel()이 불리는것을 방지
                //cancel()이 만약 호출된다면 socket이 닫혀버리므로 스트림이 형성되지 않는다.
            }

            connected(mSocket);

        }

        public void cancel() {
            try {
                if (mSocket != null) {
                    mSocket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    //서버와 클라이언트가 공용으로 쓰는 쓰레드
    private class ConnectedThread extends Thread {
        private Socket mSocket;
        private InputStream mInStream;
        private OutputStream mOutStream;

        public ConnectedThread(Socket socket) {
            mSocket = socket;
            //InputStream 과 OuputStream 얻기
            try {
                mInStream = mSocket.getInputStream();
                mOutStream = mSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            super.run();
            byte[] buffer = new byte[1024];//한번에 받는 최대 메시지 용량 1kb
            int bytes;//받은 바이트 수
            while (mState == STATE_CONNECTED) {
                try {
                    bytes = mInStream.read(buffer); //블락킹 콜
                    if (bytes == -1) {//상대방 소켓의 비정상적인 종료로 인해 아무런 메시지도 넘어 오지 않을땐 -1을 반환하게된다
                        ChatConnection.this.start();//서버 재시작
                        break;
                    }
                    mHandler.obtainMessage(Constant.MESSAGE_READ, bytes, -1, buffer).sendToTarget();//핸들러를 통해 메인쓰레드에게 메세지 전송, 두번째 인자는 안쓰므로 -1
                } catch (IOException e) {
                    e.printStackTrace();
                    connectionLost();
                    break;
                }
            }
        }

        public void write(byte[] buffer) {
            try {
                mOutStream.write(buffer);

                //메인쓰레드에게 보낸 데이터를 공유하기 위해 핸들러를 통해 한번 더 전송
                mHandler.obtainMessage(Constant.MESSAGE_WRITE, -1, -1, buffer).sendToTarget();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void cancel() {
            try {
                mSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
