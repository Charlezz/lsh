package com.charlezz.chattest;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.chat)
    TextView chat;
    @BindView(R.id.input_message)
    EditText inputMsg;
    @BindView(R.id.show_ip)
    TextView showIP;

    ChatConnection mConnection;

    //ChatConnection에서 이 핸들러를 통해 보내는 메시지는 아래의 handleMessage로 모두 들어오게 된다.
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case Constant.MESSAGE_READ:
                    chat.append("\n상대방:" + new String((byte[]) msg.obj, 0, msg.arg1));
                    break;
                case Constant.MESSAGE_WRITE:
                    chat.append("\n나:" + new String((byte[]) msg.obj));
                    break;
                case Constant.MESSAGE_STATE_CHANGED:
                    switch (msg.arg1) {
                        case ChatConnection.STATE_NONE:
                            getSupportActionBar().setTitle("NONE");
                            break;
                        case ChatConnection.STATE_LISTEN:
                            getSupportActionBar().setTitle("Listen");
                            chat.append("\n--연결대기상태--");
                            break;
                        case ChatConnection.STATE_CONNNECTING:
                            getSupportActionBar().setTitle("Connecting");
                            chat.append("\n--연결중--");
                            break;
                        case ChatConnection.STATE_CONNECTED:
                            getSupportActionBar().setTitle("Connected");
                            chat.append("\n--연결됨--");
                            break;
                    }
                    break;
                case Constant.MESSAGE_TOAST:
                    Toast.makeText(MainActivity.this, (String) msg.obj, Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        try {
            showIP.setText(getLocalIpAddress());
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (InetAddress addr : getLocalInetAddress()) {
                        Log.e(TAG, addr.getHostName());
                    }
                }
            }).start();

        } catch (Exception e) {
            e.printStackTrace();
        }
        mConnection = new ChatConnection(mHandler);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mConnection.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mConnection.stop();
    }

    @OnClick(R.id.send)
    public void send() {
        if (mConnection.getState() != ChatConnection.STATE_CONNECTED) {
            Toast.makeText(MainActivity.this, "not connected yet", Toast.LENGTH_SHORT).show();
            return;
        }
        String message = inputMsg.getText().toString();
        byte[] rawMsg = message.getBytes();
        mConnection.write(rawMsg);
        inputMsg.getText().clear();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.connect:

                final View view = LayoutInflater.from(this).inflate(R.layout.input_ip_layout, null);

                new AlertDialog.Builder(this)
                        .setView(view)
                        .setPositiveButton("Connect", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                EditText ip = (EditText) view.findViewById(R.id.ip);
                                if (mConnection != null) {
                                    Log.e(TAG, "ip:" + ip.getText().toString());
                                    mConnection.connect(ip.getText().toString());
                                }
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private String getLocalIpAddress() throws Exception {
        String resultIpv6 = "";
        String resultIpv4 = "";

        for (Enumeration en = NetworkInterface.getNetworkInterfaces();
             en.hasMoreElements(); ) {

            NetworkInterface intf = (NetworkInterface) en.nextElement();
            for (Enumeration enumIpAddr = intf.getInetAddresses();
                 enumIpAddr.hasMoreElements(); ) {

                InetAddress inetAddress = (InetAddress) enumIpAddr.nextElement();
                if (!inetAddress.isLoopbackAddress()) {
                    if (inetAddress instanceof Inet4Address) {
                        resultIpv4 = inetAddress.getHostAddress().toString();
                    } else if (inetAddress instanceof Inet6Address) {
                        resultIpv6 = inetAddress.getHostAddress().toString();
                    }
                }
            }
        }
        return ((resultIpv4.length() > 0) ? resultIpv4 : resultIpv6);
    }

    public static InetAddress[] getLocalInetAddress() {
        ArrayList<InetAddress> addresses = new ArrayList<InetAddress>();
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        addresses.add(inetAddress);
                    }
                }
            }
        } catch (SocketException ex) {
            String LOG_TAG = null;
            System.out.println(LOG_TAG + " : " + ex.toString());
        }
        return addresses.toArray(new InetAddress[0]);
    }
}
