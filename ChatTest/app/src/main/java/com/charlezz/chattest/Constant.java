package com.charlezz.chattest;

/**
 * Created by Charles on 2017. 5. 7..
 */

public class Constant {
    public static final int MESSAGE_READ = 0;
    public static final int MESSAGE_WRITE = 1;
    public static final int MESSAGE_STATE_CHANGED = 2;
    public static final int MESSAGE_TOAST = 3;
}
