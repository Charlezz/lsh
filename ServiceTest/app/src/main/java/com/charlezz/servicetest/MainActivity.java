package com.charlezz.servicetest;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    public static final String TAG = MainActivity.class.getSimpleName();

    Button start, stop, check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        start = (Button) findViewById(R.id.start);
        stop = (Button) findViewById(R.id.stop);
        check = (Button) findViewById(R.id.check);


        start.setOnClickListener(this);
        stop.setOnClickListener(this);
        check.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (start.equals(v)) {
            Log.e(TAG, "start clicked");

            Intent intent = new Intent(MainActivity.this, MyService.class);
            startService(intent);

        } else if (v.equals(stop)) {
            Log.e(TAG, "stop clicked");
            Intent intent = new Intent(MainActivity.this, MyService.class);
            stopService(intent);

        } else if (v.equals(check)) {
            Log.e(TAG, "check clicked");
            ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            boolean isRunningService = false;

            List<ActivityManager.RunningServiceInfo> list = manager.getRunningServices(Integer.MAX_VALUE);

            for (int i = 0; i < list.size(); i++) {
                ActivityManager.RunningServiceInfo info = list.get(i);
                String serviceClassName = info.service.getClassName();

                if (serviceClassName.equals(MyService.class.getName())) {
                    isRunningService = true;
                    break;
                }
            }
//            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
//                String serv = service.service.getClassName();
//                if (serv.equals(MyService.class.getName())) {
//                    isRunningService = true;
//                    break;
//                }
//            }
            if (isRunningService) {
                Log.e(TAG, "Service is running!!");
            } else {
                Log.e(TAG, "Service is NOT running!!");
            }
        }
    }
}
