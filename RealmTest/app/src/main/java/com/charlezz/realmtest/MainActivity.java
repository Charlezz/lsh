package com.charlezz.realmtest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    private static final int GROUP_DEFAULT_ID = 0;
    private static final int MENU_SIGNUP = 0;
    private static final int MENU_REFRESH = 1;

    @BindView(R.id.listView)
    ListView listView;

    ArrayAdapter<String> mAdapter;

    Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        unbinder = ButterKnife.bind(this);

        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        listView.setAdapter(mAdapter);
    }


    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item = menu.add(GROUP_DEFAULT_ID, MENU_SIGNUP, MENU_SIGNUP, "Sign Up");
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        MenuItem refreshItem = menu.add(GROUP_DEFAULT_ID, MENU_REFRESH, MENU_REFRESH, "Refresh");
        refreshItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getGroupId()) {
            case GROUP_DEFAULT_ID:
                switch (item.getItemId()) {
                    case MENU_SIGNUP:
                        startActivity(new Intent(MainActivity.this, SignUpActivity.class));
                        break;
                    case MENU_REFRESH:
                        refresh();
                        break;
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void refresh() {
        mAdapter.clear();
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Member> query = realm.where(Member.class);
        RealmResults<Member> members = query.findAll();

        for (Member member : members) {
            StringBuffer sb = new StringBuffer();
            sb.append(member.getId());
            sb.append("\n");
            sb.append(member.getPassword());

            mAdapter.add(sb.toString());
        }
    }
}
