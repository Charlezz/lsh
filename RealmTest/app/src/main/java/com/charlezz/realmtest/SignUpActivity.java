package com.charlezz.realmtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;

public class SignUpActivity extends AppCompatActivity {

    @BindView(R.id.id)
    EditText id;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.password2)
    EditText password2;


    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        unbinder = ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.save)
    public void save() {
        String strId = id.getText().toString();
        String strPassword = password.getText().toString();
        String strPassword2 = password2.getText().toString();

        Realm realm = Realm.getDefaultInstance();

        Member member = realm.where(Member.class).equalTo("id", strId).findFirst();

        if (member != null && member.getId().equals(strId)) {
            Toast.makeText(SignUpActivity.this, strId + " already exist", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!strPassword.equals(strPassword2)) {
            Toast.makeText(SignUpActivity.this, "password is incorrect", Toast.LENGTH_SHORT).show();
            return;
        }


        realm.beginTransaction();

        member = realm.createObject(Member.class, strId);
        member.setPassword(strPassword);

        realm.commitTransaction();
        finish();
    }
}
