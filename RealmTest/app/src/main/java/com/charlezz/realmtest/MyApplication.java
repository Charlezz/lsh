package com.charlezz.realmtest;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by Charles on 2017. 2. 26..
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}
