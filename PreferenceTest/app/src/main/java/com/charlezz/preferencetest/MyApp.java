package com.charlezz.preferencetest;

import android.app.Application;
import android.content.Context;

/**
 * Created by Charles on 2017. 3. 12..
 */

public class MyApp extends Application {

    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }
}
