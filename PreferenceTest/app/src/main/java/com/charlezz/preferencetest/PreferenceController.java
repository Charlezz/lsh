package com.charlezz.preferencetest;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Charles on 2017. 3. 12..
 */

class PreferenceController {

    private static final PreferenceController ourInstance = new PreferenceController();

    static PreferenceController getInstance() {
        return ourInstance;
    }


    private Context context;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private PreferenceController() {
        context = MyApp.getContext();
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = preferences.edit();
    }

    private static final String KEY_ANYTHING = "anything";

    public void setAnything(String anything) {
        editor.putString(KEY_ANYTHING, anything);
        editor.commit();
    }

    public String getAnything() {
        return preferences.getString(KEY_ANYTHING, "");
    }


    private static final String KEY_ANYTHING2 = "anything2";

    public void setAnything2(String anything) {
        editor.putString(KEY_ANYTHING2, anything);
        editor.commit();
    }

    public String getAnything2() {
        return preferences.getString(KEY_ANYTHING2, "");
    }
}
