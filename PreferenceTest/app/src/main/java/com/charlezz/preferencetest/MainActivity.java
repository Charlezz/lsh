package com.charlezz.preferencetest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.editText)
    EditText editText;
    @BindView(R.id.editText2)
    EditText editText2;

    Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }


    @OnClick(R.id.save)
    public void OnSaveClick() {
        PreferenceController.getInstance().setAnything(editText.getText().toString());
    }

    @OnClick(R.id.load)
    public void OnLoadClick() {
        editText.setText(PreferenceController.getInstance().getAnything());
    }

    @OnClick(R.id.save2)
    public void OnSaveClick2() {
        PreferenceController.getInstance().setAnything2(editText2.getText().toString());
    }

    @OnClick(R.id.load2)
    public void OnLoadClick2() {
        editText2.setText(PreferenceController.getInstance().getAnything2());
    }
}
