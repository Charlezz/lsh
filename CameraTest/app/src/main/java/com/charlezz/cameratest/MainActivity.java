package com.charlezz.cameratest;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.charlezz.cameratest.R.id.photo;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    @BindView(photo)
    ImageView photoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Log.i(TAG, "This device has a camera");
        } else {
            finish();
        }


        new TedPermission(this)
                .setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {

                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                        finish();
                    }
                })
                .check();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private static final int REQ_THUMNAIL = 1;
    private static final int REQ_FULLSIZE = 2;
    private static final int REQ_GALLERY = 3;
    private static final int REQ_SAVE_PHOTO_INTO_EXTERNAL = 4;

    private String mPhotoPath;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.thumnail:
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, REQ_THUMNAIL);

                }
                break;
            case R.id.full_size:
                Intent fullSizeIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (fullSizeIntent.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (photoFile != null) {
                        Uri photoURI = FileProvider.getUriForFile(this, "com.charlezz.cameratest.fileprovider", photoFile);

                        List<ResolveInfo> resolveInfos = getPackageManager().queryIntentActivities(fullSizeIntent, PackageManager.MATCH_DEFAULT_ONLY);

                        for (ResolveInfo info : resolveInfos) {
                            String packageName = info.activityInfo.packageName;
                            grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        }
                        fullSizeIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(fullSizeIntent, REQ_FULLSIZE);
                    }
                }
                break;
            case R.id.gallery:
                Intent galleryIntent = new Intent(Intent.ACTION_PICK);
                galleryIntent.setType(MediaStore.Images.Media.CONTENT_TYPE);
                startActivityForResult(galleryIntent, REQ_GALLERY);
                break;
            case R.id.external:
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "test.jpg")));
                startActivityForResult(takePictureIntent, REQ_SAVE_PHOTO_INTO_EXTERNAL);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    Uri photoUri;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_THUMNAIL && resultCode == RESULT_OK) {
            Bundle bundle = data.getExtras();
            Bitmap imageBitmap = (Bitmap) bundle.get("data");
            photoView.setImageBitmap(imageBitmap);
        } else if (requestCode == REQ_FULLSIZE && resultCode == RESULT_OK) {
            Uri picture = data.getData();
            photoView.setImageURI(picture);
        } else if (requestCode == REQ_FULLSIZE && resultCode != RESULT_OK) {
            deleteFile();
        } else if (requestCode == REQ_GALLERY && resultCode == RESULT_OK) {
            Uri picture = data.getData();
            photoView.setImageURI(picture);
        } else if (requestCode == REQ_SAVE_PHOTO_INTO_EXTERNAL && resultCode == RESULT_OK) {
//            Uri picture = data.getData();
//            photoView.setImageURI(picture);
            //사진이 클 경우메모리 부족

            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri uri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "test.jpg"));
            mediaScanIntent.setData(uri);
            sendBroadcast(mediaScanIntent);

        }

    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMG_" + timeStamp;
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void deleteFile() {
        File file = new File(mPhotoPath);
        if (file.exists()) {
            file.delete();
            mPhotoPath = null;
        }
    }

}
