package com.charles.basiclistview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    private ListView listView;
    private ArrayList<Integer> items = new ArrayList<>();
    private ArrayAdapter<String> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //xml에서 view 가져오기
        listView = (ListView) findViewById(R.id.listView);
        //더미 데이터 생성
        initData();
        //어댑터 생성
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        //어댑터 연결
        listView.setAdapter(mAdapter);
        //데이터를 어댑터에 연결

        for (int i = 0; i < items.size(); i++) {
            //귀찮지만..미리만들어논 데이터셋을 한개씩 어댑터에 삽입..
            mAdapter.add("Number : " + items.get(i));
        }


    }


    public void initData() {
        for (int i = 1; i <= 100; i++) {
            items.add(i);
        }
    }
}
