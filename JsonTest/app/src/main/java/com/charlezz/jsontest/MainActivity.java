package com.charlezz.jsontest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    private static final String KEY_STRING = "string";
    private static final String KEY_INTEGER = "integer";
    private static final String KEY_BOOLEAN = "boolean";

    private Unbinder unbinder;


    @BindView(R.id.plain_text)
    EditText plainText;

    @BindView(R.id.number)
    EditText number;

    @BindView(R.id.result)
    TextView result;

    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }


    @OnClick(R.id.convert)
    public void OnConvertClick() {

        String str = plainText.getText().toString();
        int num = Integer.parseInt(number.getText().toString());
        boolean b = false;

        switch (radioGroup.getCheckedRadioButtonId()) {
            case R.id._true:
                b = true;
                break;
            case R.id._false:
                b = false;
                break;
        }

        Log.e(TAG, "str:" + str);
        Log.e(TAG, "num:" + num);
        Log.e(TAG, "b:" + b);

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(KEY_STRING, str);
            jsonObject.put(KEY_INTEGER, num);
            jsonObject.put(KEY_BOOLEAN, b);
//            jsonObject.put("obj", new CustomData(10, "thomas"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        result.setText(jsonObject.toString());

        plainText.setText("");
        number.setText("");
        radioGroup.clearCheck();
    }

    @OnClick(R.id.revert)
    public void OnRevertClick() {
        Log.e(TAG, "OnRevertClick");

        String resultText = result.getText().toString();


        try {
            JSONObject resultObject = new JSONObject(resultText);
            String str = resultObject.getString(KEY_STRING);
            boolean b = resultObject.getBoolean(KEY_BOOLEAN);
            int num = resultObject.getInt(KEY_INTEGER);
//
//            CustomData data = (CustomData) resultObject.get("obj");
//            Log.e(TAG, "data.age:" + data.age);
//            Log.e(TAG, "data.name:" + data.name);

            plainText.setText(str);
            number.setText("" + num);

            if (b) {
                radioGroup.check(R.id._true);
            } else {
                radioGroup.check(R.id._false);
            }

            result.setText("");

        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(MainActivity.this, "Syntax Error", Toast.LENGTH_SHORT).show();
        }
    }

//    class CustomData {
//        int age;
//        String name;
//
//        public CustomData(int age, String name) {
//            this.age = age;
//            this.name = name;
//        }
//    }
}