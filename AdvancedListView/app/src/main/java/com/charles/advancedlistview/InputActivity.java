package com.charles.advancedlistview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class InputActivity extends AppCompatActivity {

    private EditText editName, editNumber;
    private Button save;

    public static final String KEY_PERSON = "param_person";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        editName = (EditText) findViewById(R.id.editName);
        editNumber = (EditText) findViewById(R.id.editNumber);
        save = (Button) findViewById(R.id.save);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editName.getText().toString();
                String number = editNumber.getText().toString();
                Person person = new Person(name, number);

                Intent intent = new Intent();
                intent.putExtra(KEY_PERSON, person);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
