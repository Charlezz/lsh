package com.charles.advancedlistview;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Checkable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

import static com.charles.advancedlistview.MainActivity.TAG;

/**
 * Created by Charles on 2017. 1. 10..
 */

public class PersonView extends FrameLayout implements Checkable {

    private ImageView iv;
    private TextView name, number;
    private View view;

    public PersonView(Context context) {
        super(context);
        view = LayoutInflater.from(context).inflate(R.layout.person_view, this, true);

        iv = (ImageView) view.findViewById(R.id.iv);
        name = (TextView) view.findViewById(R.id.name);
        number = (TextView) view.findViewById(R.id.number);
    }

    public void setMyItem(Person person) {

        switch (new Random().nextInt(3)) {
            case 0:
                iv.setImageResource(android.R.color.holo_blue_dark);
                break;
            case 1:
                iv.setImageResource(android.R.color.holo_green_dark);
                break;
            case 2:
                iv.setImageResource(android.R.color.holo_purple);
                break;
        }

        name.setText(person.getName());
        number.setText(person.getNumber());

    }


    private boolean isChecked;

    @Override
    public void setChecked(boolean b) {
        isChecked = b;
        refreshDrawableState(isChecked);
    }

    @Override
    public boolean isChecked() {
        Log.e(TAG, "isChecked");
        return isChecked;
    }

    @Override
    public void toggle() {
        setChecked(!isChecked);
    }

    public void refreshDrawableState(boolean checked) {
        if (checked) {
            view.setAlpha(0.5f);
        } else {
            view.setAlpha(1.0f);
        }
    }
}
