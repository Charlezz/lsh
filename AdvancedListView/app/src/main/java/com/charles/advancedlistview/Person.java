package com.charles.advancedlistview;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Charles on 2017. 1. 10..
 */

public class Person implements Parcelable {

    public String name;
    private String number;

    public Person(String name, String number) {
        this.name = name;
        this.number = number;
    }

    public Person() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.number);
    }

    protected Person(Parcel in) {
        this.name = in.readString();
        this.number = in.readString();
    }

    public static final Parcelable.Creator<Person> CREATOR = new Parcelable.Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel source) {
            return new Person(source);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };
}
