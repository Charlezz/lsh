package com.charles.advancedlistview;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Charles on 2017. 1. 10..
 */

public class AdvancedAdapter extends BaseAdapter {

    private ArrayList<Person> items = new ArrayList<>();


    public void add(Person person) {
        items.add(person);
        notifyDataSetChanged();
    }

    public void delete(int position) {
        items.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Person getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        PersonView personView = null;

        if (convertView == null) {
            personView = new PersonView(viewGroup.getContext());
        } else {
            personView = (PersonView) convertView;
        }

        personView.setMyItem(items.get(i));

        //check view 초기화
        personView.refreshDrawableState();
        switch (((ListView) viewGroup).getChoiceMode()) {
            case ListView.CHOICE_MODE_MULTIPLE:
                personView.refreshDrawableState(true);
                break;
            default:
                personView.refreshDrawableState(false);
                break;
        }


        return personView;
    }
}
