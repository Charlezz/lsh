package com.charles.advancedlistview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    private static final int REQUEST_INPUT_ACTIVITY = 0;

    private ListView listView;
    private AdvancedAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listView);
        adapter = new AdvancedAdapter();
        listView.setAdapter(adapter);


        for (int i = 0; i < 100; i++) {
            adapter.add(new Person("홍길동:" + i, "" + i));
        }


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(MainActivity.this, adapter.getItem(i).getNumber(), Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + adapter.getItem(i).getNumber()));
//                startActivity(intent);


            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (listView.getChoiceMode() == ListView.CHOICE_MODE_NONE) {
                    //none
                    Toast.makeText(MainActivity.this, "multiple chocie mode", Toast.LENGTH_SHORT).show();
                    listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                }
//                adapter.delete(i);
                return false;
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("btn").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals("btn")) {

            if (listView.getChoiceMode() == ListView.CHOICE_MODE_MULTIPLE) {
                SparseBooleanArray array = listView.getCheckedItemPositions();

                for (int i = array.size() - 1; i >= 0; i--) {
                    int key = array.keyAt(i);
                    boolean state = array.get(key);
                    if (state) {
                        adapter.delete(key);
                    }
                }
                listView.clearChoices();
                listView.setChoiceMode(ListView.CHOICE_MODE_NONE);
            } else {
                Intent intent = new Intent(MainActivity.this, InputActivity.class);
                startActivityForResult(intent, REQUEST_INPUT_ACTIVITY);
            }

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQUEST_INPUT_ACTIVITY) {
            Person person = data.getParcelableExtra(InputActivity.KEY_PERSON);
            Log.e(TAG, "name:" + person.getName());
            Log.e(TAG, "number:" + person.getNumber());
            adapter.add(person);
        }
    }

    @Override
    public void onBackPressed() {
        if (listView.getChoiceMode() == ListView.CHOICE_MODE_MULTIPLE) {
            listView.setChoiceMode(ListView.CHOICE_MODE_NONE);
            listView.clearChoices();
            adapter.notifyDataSetChanged();
            Toast.makeText(MainActivity.this, "chocie모드 해제", Toast.LENGTH_SHORT).show();
        } else {
            super.onBackPressed();
        }
    }
}
