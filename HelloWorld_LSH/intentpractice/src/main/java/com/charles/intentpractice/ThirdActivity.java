package com.charles.intentpractice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ThirdActivity extends AppCompatActivity {

    public static final String KEY_THIRD = "key third";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        final EditText input = (EditText) findViewById(R.id.input);

        Button send = (Button) findViewById(R.id.send);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String inputTxt = input.getText().toString();

                //인텐트 생성
                Intent intent = new Intent();
                //데이터 삽입
                intent.putExtra(KEY_THIRD, inputTxt);
                //이 액티비티를 호출한 액티비티에게 전송할 결과물 설정
                setResult(Activity.RESULT_OK, intent);
                //액티비티 종료
                finish();
            }
        });

        setResult(RESULT_CANCELED);

    }
}
