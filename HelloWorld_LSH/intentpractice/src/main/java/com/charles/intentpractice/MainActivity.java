package com.charles.intentpractice;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    private Button button, call, third;
    private int count = 0;


    public static final int REQ_THIRD = 0;

    public static final String KEY_COUNT = "key count";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, SecondActivity.class);
                i.putExtra(KEY_COUNT, count);
                count++;
                startActivity(i);
            }
        });

        call = (Button) findViewById(R.id.call);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:01012345678"));
                startActivity(i);
            }
        });

        third = (Button) findViewById(R.id.callThird);
        third.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ThirdActivity.class);
                startActivityForResult(i, REQ_THIRD);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && requestCode == REQ_THIRD) {
            String str = data.getStringExtra(ThirdActivity.KEY_THIRD);
            Toast.makeText(MainActivity.this, str, Toast.LENGTH_LONG).show();
        }


    }
}
