package com.charles.intentpractice;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        TextView tv = (TextView) findViewById(R.id.textView);

        Intent intent = getIntent();

        int count = intent.getIntExtra(MainActivity.KEY_COUNT, -1);

        tv.setText(String.valueOf(count));


    }
}
