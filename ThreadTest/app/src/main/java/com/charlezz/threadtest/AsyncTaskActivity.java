package com.charlezz.threadtest;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AsyncTaskActivity extends AppCompatActivity {
    @BindView(R.id.start)
    Button start;
    @BindView(R.id.stop)
    Button stop;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    boolean isProgressing;
    Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task);
        unbinder = ButterKnife.bind(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }


    @OnClick(R.id.start)
    public void OnStartClick() {
        isProgressing = true;

        AsyncTask<Integer, Integer, Integer> task = new AsyncTask<Integer, Integer, Integer>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Toast.makeText(AsyncTaskActivity.this, "다운로드 시작", Toast.LENGTH_SHORT).show();
                progressBar.setProgress(0);
            }

            @Override
            protected Integer doInBackground(Integer... params) {
//                Log.e(TAG, "doInBackground:" + (Looper.myLooper() == Looper.getMainLooper()));
                isProgressing = true;
                int initCount = params[0];
                while (isProgressing) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                    if (initCount >= 100) {
                        break;
                    }
                    initCount++;
                    publishProgress(initCount);
                }
                return initCount;
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);

                progressBar.setProgress(values[0]);
            }

            @Override
            protected void onPostExecute(Integer integer) {
                super.onPostExecute(integer);
//                Log.e(TAG, "onPostExecute:" + (Looper.myLooper() == Looper.getMainLooper()));
                Toast.makeText(AsyncTaskActivity.this, "다운로드 끝", Toast.LENGTH_SHORT).show();
            }
        };
        task.execute(10);

    }

    @OnClick(R.id.stop)
    public void OnStopClick() {
        isProgressing = false;
    }

    public static final String TAG = AsyncTaskActivity.class.getSimpleName();

}
