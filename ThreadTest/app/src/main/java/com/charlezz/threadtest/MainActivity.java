package com.charlezz.threadtest;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.start)
    Button start;
    @BindView(R.id.stop)
    Button stop;
    @BindView(R.id.tv)
    TextView tv;

    int count = 0;
    boolean isProgressing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

    }

    Unbinder unbinder;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }


    @OnClick(R.id.start)
    public void OnStartClick() {
        isProgressing = true;
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                //여기는 서브스레드 안
                while (isProgressing) {
                    count++;
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            //여기는 메인스레드 , 뷰 갱신가능
//                            tv.setText("" + count);
//                        }
//                    });


//                    handler.obtainMessage().sendToTarget();
                    handler.sendEmptyMessage(count);
                }
            }
        }, "My Thread");
        t.start();
    }

    @OnClick(R.id.stop)
    public void OnStopClick() {
        isProgressing = false;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            //main thread, ui갱신가능 이아래부터는 .

            Log.e(TAG, "msg:" + msg.what);
//            TextView tv = (TextView) findViewById(R.id.tv);
//            tv.setText(msg.what);
            tv.setText("" + msg.what);
        }
    };

    public static final String TAG = MainActivity.class.getSimpleName();


}
