package com.example;

/**
 * Created by Charles on 2017. 2. 5..
 */

public class MyThread extends Thread {
    String name;

    public MyThread(String name) {
        this.name = name;
    }

    public void run() {
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
//            System.out.println(name + " (우선 순위: " + getPriority() + ")");
            try {
                sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(name + " Ended");
    }

}
