package com.charlezz.gsontest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.gson.Gson;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    Unbinder unbinder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }


    @OnClick(R.id.btn)
    public void OnClick() {

        CustomData data = new CustomData("Charles", 30, false);
        String jsonData = new Gson().toJson(data);
        Log.e(TAG, "jsonData:" + jsonData);


        CustomData2 data2 = new Gson().fromJson(jsonData, CustomData2.class);

//        Log.e(TAG, "data.hasCar:" + data2.hasCar);
//        Log.e(TAG, "data.number:" + data2.number);
        Log.e(TAG, "data.name:" + data2.name);

    }

    class CustomData {
        String name;
        int number;
        boolean hasCar;

        public CustomData(String name, int number, boolean hasCar) {
            this.name = name;
            this.number = number;
            this.hasCar = hasCar;
        }
    }

    class CustomData2 {
        String name;
    }
}
