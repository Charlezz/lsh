package com.charles.chatsample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Date;

import static android.util.Log.e;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    private ListView listView;
    private ChatAdapter adapter;

    private EditText editText;
    private Button send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        editText = (EditText) findViewById(R.id.editText);
        send = (Button) findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Message message = new Message();
                message.setMessage(editText.getText().toString());
                message.setDate(new Date());
                message.setType(Message.TYPE_ME);
                message.setId("Charles");
                adapter.add(message);
            }
        });

        listView = (ListView) findViewById(R.id.listView);
        adapter = new ChatAdapter();
        listView.setAdapter(adapter);
        adapter.setItems(generateDummy());

    }

    private ArrayList<Message> generateDummy() {
        ArrayList<Message> list = new ArrayList<>();

        for (int i = 0; i < 99; i++) {
            Message message = new Message();
            if (i % 2 == 0) {
                message.setType(Message.TYPE_ME);
                message.setId("Charles");
            } else {
                message.setType(Message.TYPE_OPPONENT);
                message.setId("LSH");
            }
            message.setMessage("Hello:" + i);
            message.setDate(new Date());
            list.add(message);
        }
        e(TAG, "listSize:" + list.size());
        return list;
    }
}
