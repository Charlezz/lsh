package com.charles.chatsample;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by Charles on 2017. 1. 15..
 */

public class ChatAdapter extends BaseAdapter {

    private ArrayList<Message> items = new ArrayList<>();

    public void add(Message msg) {
        items.add(msg);
        notifyDataSetChanged();
    }

    public void setItems(ArrayList<Message> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Message getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        View messageView = convertView;
        Message tempMessage = items.get(i);

        switch (tempMessage.getType()) {
            case Message.TYPE_ME:
                if (convertView == null) {
                    messageView = new MessageView0(viewGroup.getContext());
                } else {
                    if (convertView instanceof MessageView0) {
                        messageView = convertView;
                    } else {
                        messageView = new MessageView0(viewGroup.getContext());
                    }
                }
                ((MessageView0) messageView).setMessage(tempMessage);
                break;
            case Message.TYPE_OPPONENT:
                if (convertView == null) {
                    messageView = new MessageView1(viewGroup.getContext());
                } else {
                    if (convertView instanceof MessageView1) {
                        messageView = convertView;
                    } else {
                        messageView = new MessageView1(viewGroup.getContext());
                    }
                }
                ((MessageView1) messageView).setMessage(tempMessage);
                break;
        }

        return messageView;
    }
}
