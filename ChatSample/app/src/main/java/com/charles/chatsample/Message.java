package com.charles.chatsample;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by Charles on 2017. 1. 15..
 */

public class Message implements Parcelable {


    public static final int TYPE_ME = 0;
    public static final int TYPE_OPPONENT = 1;

    private int type;
    private String message;
    private String id;
    private Date date;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type);
        dest.writeString(this.message);
        dest.writeString(this.id);
        dest.writeLong(this.date != null ? this.date.getTime() : -1);
    }

    public Message() {
    }

    protected Message(Parcel in) {
        this.type = in.readInt();
        this.message = in.readString();
        this.id = in.readString();
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
    }

    public static final Parcelable.Creator<Message> CREATOR = new Parcelable.Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel source) {
            return new Message(source);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };
}
