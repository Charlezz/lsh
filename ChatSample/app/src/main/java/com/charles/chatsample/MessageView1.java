package com.charles.chatsample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by Charles on 2017. 1. 15..
 */

public class MessageView1 extends FrameLayout {

    private Message message;

    private ImageView profileImage;
    private TextView name, msg, date;

    public MessageView1(Context context) {
        super(context);

        View view = LayoutInflater.from(context).inflate(R.layout.message_view_1, this, true);

        profileImage = (ImageView) view.findViewById(R.id.profileImage);
        name = (TextView) view.findViewById(R.id.name);
        msg = (TextView) view.findViewById(R.id.msg);
        date = (TextView) view.findViewById(R.id.date);

    }

    public void setMessage(Message message) {
        this.message = message;

        name.setText(message.getId());
        msg.setText(message.getMessage());
        SimpleDateFormat sdf = new SimpleDateFormat("aaa hh:mm", Locale.KOREA);
        date.setText(sdf.format(message.getDate()));
    }
}
