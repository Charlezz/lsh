package com.charles.customlistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by Charles on 2017. 1. 1..
 */

public class CustomView extends FrameLayout {
    private TextView tv;

    //framelayout상속시 기본생성자가 반드시 하나이상있어야함
    public CustomView(Context context) {
        super(context);
        //xml -> android view, 인플레이터 필요
        View view = LayoutInflater.from(context).inflate(R.layout.custom_view, this, true);
        tv = (TextView) view.findViewById(R.id.tv);
    }

    public void setMyData(String str) {
        tv.setText(str);
    }
}
