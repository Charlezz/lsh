package com.charles.customlistview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private ListView listView;

    private ArrayList<String> items = new ArrayList<>();

    private CustomAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //리스트뷰 xml 로 만든것 가져오기
        listView = (ListView) findViewById(R.id.listView);
        //데이터 생성
        InitData();
        //어댑터 생성
        mAdapter = new CustomAdapter();
        //리스트뷰 연결
        listView.setAdapter(mAdapter);
        //데이터 연결
        mAdapter.setItems(items);

    }

    private void InitData() {
        for (int i = 0; i < 100; i++) {
            items.add(String.valueOf(new Random().nextInt(100)));
        }
    }

}
