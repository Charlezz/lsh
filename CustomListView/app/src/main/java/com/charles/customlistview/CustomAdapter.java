package com.charles.customlistview;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by Charles on 2017. 1. 1..
 */
//커스텀어댑터를 만드려면 베이스어댑터를 상속한다
//상속하면 4개의 메소드를 오버라이드 하여 반드시 구현해야만한다
public class CustomAdapter extends BaseAdapter {

    private ArrayList<String> items = new ArrayList<>();

    public void setItems(ArrayList<String> items) {
        this.items = items;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public String getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        CustomView view = null;
        if (convertView == null) {
            view = new CustomView(viewGroup.getContext());
        } else {
            view = (CustomView) convertView;
        }

        view.setMyData(items.get(position));

        return view;
    }
}
