package com.charlezz.alarmtest;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Charles on 2017. 3. 5..
 */
public class AlarmController {

    private static final int REQ_ALARM_DEFAULT = 0;

    public static final String TAG = AlarmController.class.getSimpleName();

    private static AlarmController ourInstance = new AlarmController();

    public static AlarmController getInstance() {
        return ourInstance;
    }

    private Context context;
    private AlarmManager am;
    private PendingIntent pi;

    private AlarmController() {
        context = MyApp.getContext();
        am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }


    public void registerAlarm(long ms) {
        Intent intent = new Intent(context, AlarmActivity.class);
        pi = PendingIntent.getActivity(context, REQ_ALARM_DEFAULT, intent, PendingIntent.FLAG_ONE_SHOT);
        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + ms, pi);

//        context.sendBroadcast(intent);
//        PendingIntent.getBroadcast()
    }

    public void unregisterAlarm() {
        if (pi != null) {
            am.cancel(pi);
        }

    }
}
