package com.charlezz.alarmtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class AlarmActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MediaPlayerController.getInstance().start();
        VibrationController.getInstance().invokeVibration();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MediaPlayerController.getInstance().stop();
        VibrationController.getInstance().cancel();
    }
}
