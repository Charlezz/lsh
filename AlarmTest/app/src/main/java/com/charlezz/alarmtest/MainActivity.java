package com.charlezz.alarmtest;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Switch;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    Unbinder unbinder;

    @BindView(R.id.picker)
    TimePicker mPicker;

    @BindView(R.id.set_alarm)
    Switch alramSwitch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnCheckedChanged(R.id.set_alarm)
    public void onAlarmSwitchChanged(boolean enable) {
        Locale currentLocale = getCurrentLocale();
        Calendar currentCalendar = Calendar.getInstance(currentLocale);

        //current Time
        int currentHour = currentCalendar.get(currentCalendar.HOUR_OF_DAY);
        int currentMinute = currentCalendar.get(Calendar.MINUTE);

        //picker Time
        int hour, minute;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            hour = mPicker.getHour();
            minute = mPicker.getMinute();
        } else {
            hour = mPicker.getCurrentHour();
            minute = mPicker.getCurrentMinute();
        }

        Calendar pickerCalendar = Calendar.getInstance(currentLocale);

        pickerCalendar.set(Calendar.HOUR_OF_DAY, hour);
        pickerCalendar.set(Calendar.MINUTE, minute);

        long currentTimeInMil = currentCalendar.getTimeInMillis();
        long pickerTimeInMil = pickerCalendar.getTimeInMillis();

        Log.e(TAG, "currentTimeInMil:" + currentTimeInMil);
        Log.e(TAG, "pickerTimeInMil:" + pickerTimeInMil);

        if (pickerTimeInMil > currentTimeInMil) {
            long diff = pickerTimeInMil - currentTimeInMil;
            AlarmController.getInstance().registerAlarm(diff);
            Log.e(TAG, "diff:" + diff);
        } else if (currentTimeInMil > pickerTimeInMil) {
            long diff = currentTimeInMil - pickerTimeInMil;
            diff = AlarmManager.INTERVAL_DAY - diff;
            AlarmController.getInstance().registerAlarm(diff);
        }

    }

    @TargetApi(Build.VERSION_CODES.N)
    public Locale getCurrentLocale() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return getResources().getConfiguration().getLocales().get(0);
        } else {
            //noinspection deprecation
            return getResources().getConfiguration().locale;
        }
    }

}
