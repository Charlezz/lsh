package com.charlezz.alarmtest;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;

import java.io.IOException;

/**
 * Created by Charles on 2017. 3. 5..
 */
public class MediaPlayerController {

    public static final String TAG = MediaPlayerController.class.getSimpleName();

    private static MediaPlayerController ourInstance = new MediaPlayerController();

    public static MediaPlayerController getInstance() {
        return ourInstance;
    }

    private MediaPlayer mPlayer;

    private MediaPlayerController() {
    }

    public void start() {
        mPlayer = new MediaPlayer();
        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);

        try {
            mPlayer.setDataSource(MyApp.getContext(), alert);
            mPlayer.setAudioStreamType(AudioManager.STREAM_RING);
            mPlayer.setLooping(true);
            mPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mPlayer.start();
    }

    public void stop() {
        mPlayer.stop();
        mPlayer.release();
        mPlayer = null;
    }
}
