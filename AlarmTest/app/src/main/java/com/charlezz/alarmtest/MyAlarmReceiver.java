package com.charlezz.alarmtest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import static android.content.ContentValues.TAG;

/**
 * Created by Charles on 2017. 3. 12..
 */

public class MyAlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            // TODO: 2017. 3. 12. set alarm again
            Log.e(TAG, "ACTION_BOOT_COMPLETED");
        }
    }
}
